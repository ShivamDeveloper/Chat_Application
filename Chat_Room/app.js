const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var ejs =require("ejs");

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());
const compression = require("compression");
const { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require('constants');
app.set('views', path.join(__dirname, 'view'));
app.set("view engine", "ejs");

app.get('/', function(req, res) {
    
    res.render('index.ejs');
});
var select=['Ankit',"GANESH"];
var chat_Room=[{
    GroupA:{
        TeamName:select[0],
        users:[{username:"Shyam"},{username:"Rohit"},{username:"Ram"},{username:"Shiv"},{username:"Krishan"}]
    },
    GroupB:{
        TeamName:select[1],
        users:[{username:"Karan"},{username:"Mohit"},{username:"Parth"},{username:"Shyam"},{username:"Rohit"}]
    }
}];
io.sockets.on('connection', function(socket) {
  
    socket.on('username', function(username) {
        socket.username = username;

        for(let i=0; i<5; i++)
        {
            if(chat_Room[0].GroupA.users[i].username==username)
            {
                io.emit('is_online', '🔵 <i>' + socket.username + ' join the chat..</i>');
                return false;
            }
            else if(chat_Room[0].GroupB.users[i].username==username){
                io.emit('is_online', '🔵 <i>' + socket.username + ' join the chat..</i>');
                return false;
            }
        }
        io.emit('is_online','🔵 not from Any Group');
    });
    socket.on('Group', function(Group) {
        socket.Group = Group;
        
       
        if(select.includes(Group))
        {
            console.log("Selected Group is:",socket.Group,chat_Room);
            console.log("Team name is:",chat_Room[0].GroupA);
  
                if(chat_Room[0].GroupA.TeamName==socket.Group)
                {
                    for(let i=0; i<chat_Room[0].GroupA.users.length; i++)
                    {
                        io.emit('Group', 'welcome to group::'+socket.Group+"with users:"+chat_Room[0].GroupA.users[i].username);
                    }
                }
                if(chat_Room[0].GroupB.TeamName==socket.Group)
                {
                    for(let i=0; i<chat_Room[0].GroupB.users.length; i++)
                    {
                        io.emit('Group', 'welcome to group::'+socket.Group+"with users:"+chat_Room[0].GroupB.users[i].username);
             
                    }
                }
        }
        else{
            io.emit('Group', 'Sorry not a part of this group or Group does not exist. You can chat Randomly with Anyone::'+socket.Group );
        }
    });
    socket.on('disconnect', function(username) {
        io.emit('is_online', '🔴 <i>' + socket.username + ' left the chat..</i>');
    });
   

    socket.on('chat_message', function(message) {
        io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message);
    });

});

const server = http.listen(8080, function() {
    console.log('listening on *:8080');
});